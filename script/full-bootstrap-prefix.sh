#!/usr/bin/env bash

# Local gentoo mirror, or default to main, eg.
#
export GENTOO_MIRROR="ftp://192.168.100.9/pub/gentoo/distfiles"

# If there is no GENTOO_MIRROR set, then default to our main distfiles server
#
export GENTOO_MIRROR=${GENTOO_MIRROR:="http://distfiles.gentoo.org/distfiles"}

# Installation location
# 
export EPREFIX="$HOME/gentoo"
export PATH="$EPREFIX/usr/bin:$EPREFIX/bin:$EPREFIX/tmp/usr/bin:$EPREFIX/tmp/bin:$PATH"

mkdir -p $EPREFIX/tmp

[ ! -x ${EPREFIX}/tmp/usr/bin/bash ] && ./bootstrap-bash.sh $EPREFIX/tmp
[ ! -d ${EPREFIX}/usr/portage ]      && ./bootstrap-prefix.sh $EPREFIX latest_tree

[ ! -x ${EPREFIX}/tmp/usr/bin/make      ] && ./bootstrap-prefix.sh $EPREFIX/tmp make
[ ! -x ${EPREFIX}/tmp/usr/bin/wget      ] && ./bootstrap-prefix.sh $EPREFIX/tmp wget
[ ! -x ${EPREFIX}/tmp/usr/bin/sed       ] && ./bootstrap-prefix.sh $EPREFIX/tmp sed
[ ! -x ${EPREFIX}/tmp/usr/bin/python2.6 ] && ./bootstrap-prefix.sh $EPREFIX/tmp python
[ ! -x ${EPREFIX}/tmp/usr/bin/cp        ] && ./bootstrap-prefix.sh $EPREFIX/tmp coreutils8
[ ! -x ${EPREFIX}/tmp/usr/bin/find      ] && ./bootstrap-prefix.sh $EPREFIX/tmp findutils3
[ ! -x ${EPREFIX}/tmp/usr/bin/tar       ] && ./bootstrap-prefix.sh $EPREFIX/tmp tar22
[ ! -x ${EPREFIX}/tmp/usr/bin/patch     ] && ./bootstrap-prefix.sh $EPREFIX/tmp patch9
[ ! -x ${EPREFIX}/tmp/usr/bin/gawk      ] && ./bootstrap-prefix.sh $EPREFIX/tmp gawk
[ ! -x ${EPREFIX}/tmp/usr/bin/grep      ] && ./bootstrap-prefix.sh $EPREFIX/tmp grep
[ ! -x ${EPREFIX}/tmp/usr/bin/m4        ] && ./bootstrap-prefix.sh $EPREFIX/tmp m4
[ ! -x ${EPREFIX}/tmp/usr/bin/bison     ] && ./bootstrap-prefix.sh $EPREFIX/tmp bison

if [ ! -x ${EPREFIX}/usr/bin/emerge        ]; then
    #Make a profile for us
    cp -R ${EPREFIX}/usr/portage/profiles/prefix/bsd/openbsd/4.{2,8}
    cp -R ${EPREFIX}/usr/portage/profiles/prefix/bsd/openbsd/4.{2,9}
    for arch in ppc x86 x64; do
        for ver in 4.8 4.9; do
            sed -i "s/4.2/${ver}/g" ${EPREFIX}/usr/portage/profiles/prefix/bsd/openbsd/${ver}/${arch}/make.defaults
        done
    done
    ./bootstrap-prefix.sh $EPREFIX portage
fi

hash -r

# FIXME: the bootstrap scripts use GENTOO_MIRROR but emerge uses GENTOO_MIRRORS
#
export GENTOO_MIRRORS=${GENTOO_MIRROR%/distfiles}

# FIXME: Sooner or later we'll have to get our profiles straight and won't need this
#
export ACCEPT_KEYWORDS="* ~*"
[ ! -x ${EPREFIX}/bin/sed      ] && emerge -1 sed
[ ! -x ${EPREFIX}/bin/bash     ] && emerge -1 -O bash 
[ ! -x ${EPREFIX}/usr/bin/wget ] && emerge -1 -O wget

[ ! -x ${EPREFIX}/sbin/runscript.sh       ] && emerge -1 -O baselayout-prefix
[ ! -x ${EPREFIX}/usr/bin/m4              ] && emerge -1 -O m4
[ ! -x ${EPREFIX}/usr/bin/autoconf-*      ] && emerge -1 -O autoconf
[ ! -x ${EPREFIX}/usr/bin/autoconf        ] && emerge -1 -O autoconf-wrapper
[ ! -x ${EPREFIX}/usr/bin/automake-*      ] && emerge -1 -O automake
[ ! -x ${EPREFIX}/usr/bin/automake        ] && emerge -1 -O automake-wrapper
[ ! -x ${EPREFIX}/usr/bin/libtool         ] && emerge -1 -O libtool

export CONFIG_SHELL="${EPREFIX}/bin/bash"
[ ! -x ${EPREFIX}/usr/bin/xz             ] && emerge -1 -O xz-utils
unset CONFIG_SHELL

[ ! -x ${EPREFIX}/usr/bin/flex            ] && emerge -1 -O flex
[ ! -x ${EPREFIX}/usr/bin/bison           ] && emerge -1 -O bison
[ ! -x ${EPREFIX}/usr/bin/binutils-config ] && emerge -1 -O binutils-config
[ ! -x ${EPREFIX}/usr/bin/gcc-config      ] && emerge -1 -O gcc-config

#
# TODO: We've gotten up to approx code listing 1.7 of the FBSD bootstrap
# http://www.gentoo.org/proj/en/gentoo-alt/prefix/bootstrap-freebsd.xml
#
#$ emerge --oneshot --nodeps binutils
#$ emerge --oneshot --nodeps "=gcc-4.2*"




