# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/sys-devel/binutils/binutils-2.21.ebuild,v 1.2 2011/03/11 06:52:40 vapier Exp $

PATCHVER="1.0"
ELF2FLT_VER=""
inherit toolchain-binutils autotools eutils

KEYWORDS="~alpha ~amd64 ~arm ~hppa ~ia64 ~m68k ~mips ~ppc ~ppc64 ~s390 ~sh
~sparc ~x86 ~sparc-fbsd ~x86-fbsd ~x86-obsd"

src_unpack(){
	toolchain-binutils_src_unpack
	epatch "${FILESDIR}"/${P}-obsd.patch
#	TODO: Workaround autoconf version crap
	for dir in bfd opcodes binutils ld gprof gas; do
		cd "${S}"/$dir
		eautoreconf
		cd "${S}"
	done
}
