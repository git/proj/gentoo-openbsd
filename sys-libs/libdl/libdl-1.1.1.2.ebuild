# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit toolchain-funcs multilib

DESCRIPTION="Dummy libdl.so for systems where dlopen() is builtin into ld.so"
HOMEPAGE="http://www.gnu-darwin.org/shims/libdl.c"
SRC_URI="http://www.gnu-darwin.org/shims/libdl.c"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~x86 ~x86-openbsd"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_unpack(){
	cp "${DISTDIR}/${A}" "${S}" || die "cp failed"
}

src_compile(){
	$(tc-getCC) ${CFLAGS} ${LDFLAGS} libdl.c \
		-shared \
		-Wl,--soname=libdl.so.1 \
		-fPIC \
		-o libdl.so.1 || die "compilation failed"
}

src_install(){
	dolib.so libdl.so.1 || die "libdl.so failed"
	dosym libdl.so.1 /usr/$(get_libdir)/libdl.so || die "dosym failed"
}
